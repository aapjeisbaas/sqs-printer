FROM python:alpine
MAINTAINER Stein van Broekhoven <stein@aapjeisbaas.nl>

# copy code and install requirements
RUN pip install boto3 requests
COPY print-client.py /app/print-client.py

ENTRYPOINT ["python", "-u", "/app/print-client.py"]
