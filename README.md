# sqs-printer

This container subscribes to an SQS queue and downloads S3 links posted there to send them to a network printer with [pdf-to-ipp](https://gitlab.com/aapjeisbaas/pdf-to-ipp)

## Local run
```
docker run \
  -e "DESTINATION=http://pdf-to-ipp.thuis/uploader" \
  -e "QUEUE_NAME=packingslip-print" \
  -e "AWS_DEFAULT_REGION=eu-west-1" \
  -e "AWS_ACCESS_KEY_ID=AK******CB" \
  -e "AWS_SECRET_ACCESS_KEY=1P******a" \
  aapjeisbaas/sqs-printer:latest
```
## Test the sqs input
```
aws sqs send-message --queue-url https://sqs.eu-west-1.amazonaws.com/##########/packingslip-print --message-body '{"bucket": "awesome-bucketname", "key": "packingslips/116544516.pdf"}'
```
expected test output
```
Subscribing to sqs: https://eu-west-1.queue.amazonaws.com/##########/packingslip-print
2020-08-28 09:32:35.422806 Done sending pdf to printer: packingslips/116544516.pdf
```


## ENV Vars

Key  | Value | Explanation
--- | --- | ---
DESTINATION | http://pdf-to-ipp.thuis/uploader | [pdf-to-ipp](https://gitlab.com/aapjeisbaas/pdf-to-ipp) http endpoint
QUEUE_NAME | packingslip-print | The name of your sqs queue
AWS_DEFAULT_REGION | eu-west-1 | The AWS reqion you want to connect to
AWS_ACCESS_KEY_ID | AK******CB | AWS cli credentials key
AWS_SECRET_ACCESS_KEY | 1P******a | AWS cli credentials secret

---

## SQS message body
This should be a json body that follows the format below, make sure you have **no** `s3://` in the bucket and **no** leading `/` in the key.
```json
{
  "bucket": "awesome-bucketname",
  "key": "packingslips/116544516.pdf"
}
```

## How it works

The script subscribes to the queue and retrieves the messages.

For each message it downloads the file from S3 to `/tmp/sqs-printer-pdf-tmp/` and sends the file to the pdf-to-ipp endpoint.

After this the temporary file gets deleted and starts polling for new messages.