import boto3
import requests
import datetime
import json
import os
from time import sleep

queueName = os.getenv('QUEUE_NAME', default='pdf-print-queue')
httpDestination = os.getenv('DESTINATION', default='http://192.168.1.10:9898/uploader')
printer_wait_time = int(os.getenv('PRINTER_WAIT_TIME', default='30'))

s3client = boto3.client('s3')
sqsclient = boto3.client('sqs')

queues = sqsclient.list_queues(QueueNamePrefix=queueName)
queue_url = queues['QueueUrls'][0]

print("Subscribing to sqs: " + queue_url)

tmpDir = '/tmp/sqs-printer-pdf-tmp/'

try:
    os.mkdir(tmpDir)
except FileExistsError:
    pass

def get_s3_file(bucket, key):
    filename = key.split("/")[-1]
    s3client.download_file(bucket, key, tmpDir + filename)
    return str(tmpDir + filename)

def send_to_printer(pdf):
    files = {'file': open(pdf, 'rb')}
    requests.post(httpDestination, files=files)

def remove_tmp_file(pdf):
    os.remove(pdf)

# main loop
while True:
    try:
        messages = sqsclient.receive_message(QueueUrl=queue_url, MaxNumberOfMessages=5, VisibilityTimeout=60, WaitTimeSeconds=20)
        fail_count = 0
    except:
        fail_count += 1
        sleep_time = min(fail_count ** 2,60) # backoff with max of 60 secconds
        print("sqs message recieve failed, waiting for: {} seconds before retry.".format(sleep_time))
        sleep(sleep_time)
        messages = {}
    if 'Messages' in messages:
        for message in messages['Messages']: # 'Messages' is a list
            # process the messages
            body = json.loads(message['Body'])
            pdf = get_s3_file(bucket=body['bucket'], key=body['key'])
            send_to_printer(pdf)
            remove_tmp_file(pdf)
            print(str(datetime.datetime.now()) + " Done sending pdf to printer: " + body['key'])
            sqsclient.delete_message(QueueUrl=queue_url,ReceiptHandle=message['ReceiptHandle'])
            sleep(printer_wait_time)

